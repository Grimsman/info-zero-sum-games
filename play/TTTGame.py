# a generic tic-tac-toe game
import random


class TTTGame:

    ## __init__
    # intializes the class
    #
    # @param board - (TTTGameBoard) represents the game board
    # @param user_player - (TTTPlayer) a player object representing the user
    # @param cpu_player - (TTTPLayer) a player object representing the cpu player
    #
    def __init__(self, wins=True):
        self.board = None
        self.user_player = None
        self.cpu_player = None
        self.wins = wins

    ## play
    # main loop that manages game play
    #
    def play(self):

        # print the rules of the game for the user
        self.print_rules()

        # determine which player starts: 0 if the user starts
        # and 1 if the CPU starts
        self.board.turn = random.randint(0, 1)
        corr = False

        # user determines which symbol to get: X or O
        while not corr:
            inpt = raw_input('would you like to be "X" or "O"? (X/O)')
            if inpt.lower() == 'x' or inpt.lower() == 'o':
                user_sym = inpt.upper()
                corr = True
            cpu_sym = 'X'
            if cpu_sym == user_sym:
                cpu_sym = 'O'
            self.board.zero_sym = user_sym
            self.board.one_sym = cpu_sym

        # main playing loop, goes until the board reports an end state
        while not self.board.end:
            if self.board.turn == 0:
                curr_player = self.user_player
            else:
                curr_player = self.cpu_player

            play = curr_player.get_play(self.board)
            self.board.update(play)

        # print the results of the game
        self.board.print_final()
        if self.board.res == 0:
            print('you tied')
        elif self.board.turn == 0:
            if self.wins:
                print('you lose')
            else:
                print('you win')
        elif self.board.turn == 1:
            if self.wins:
                print('you win')
            else:
                print('you lose')

    ## print_rules
    # prints the rules of the game for the user
    #
    def print_rules(self):
        print('Welcome to playing Tic-Tac-Toe')
