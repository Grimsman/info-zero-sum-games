# the game board for the circular tic-tac-toe game
from TTTGameBoard import TTTGameBoard
import numpy as np


class CTTTGameBoard(TTTGameBoard):

    ## __init__
    # initializes the class
    #
    # @param size - (int) the size of the circle
    # @param hidden - (list of int) the spaces on the circle that will
    #                 be hidden from both players. the entries must be in
    #                 range(size)
    #
    # a CTTTGameBoard object additionally has the following properties:
    #
    # boards - a list of size 3 of boards. indices are as follows:
    #          0: user game board
    #          1: cpu game board
    #          2: real game board
    #          each board is a str of length self.size. it represents the
    #          board as seen by each entity listed above. for a list of
    #          the meaning of each symbol on the board, see the create_matrices
    #          notebook
    # hidden_moves - a dictionary with keys 0 and 1 and with int values. 0 represents
    #                the user and 1 the cpu. the values in the dictionary are meant to
    #                track how many times the corresponding player has played in a
    #                hidden space
    # moves - a dictionary with keys the same as hidden_moves. here the values are a
    #         list of int. this is meant to track the moves that each player has made
    #
    def __init__(self, size, hidden=[], in_a_row=3, wins=True):
        TTTGameBoard.__init__(self)
        self.size = size
        self.hidden_spaces = hidden
        self.hidden_moves = {0:0, 1:0}
        self.moves = {0:[], 1:[]}
        empty_board = ''.join(['*' for x in range(size)])
        self.boards = [empty_board for x in range(3)]
        self.in_a_row = in_a_row
        self.end_strs = []
        self.end_strs.append(''.join(['X' for x in range(in_a_row)]))
        self.end_strs.append(''.join(['O' for x in range(in_a_row)]))
        self.wins = wins


    ## update
    # updates the board with a player move
    #
    # @param move - (int) the space where the current player is playing
    #
    def update(self, move):
        me = self.turn
        you = (self.turn + 1) % 2

        # update current player board
        my_board = list(self.boards[me])
        if my_board[move] == '*':
            my_board[move] = 'I'
        elif my_board[move] == '@':
            my_board[move] = '$'

        if move in self.hidden_spaces:
            self.hidden_moves[me] += 1
        self.moves[me].append(move)

        self.boards[me] = ''.join(my_board)

        # update opponent board
        your_board = list(self.boards[you])
        if move in self.hidden_spaces:
            if self.hidden_moves[me] == 1:
                for hs in self.hidden_spaces:
                    if your_board[hs] != 'I':
                        your_board[hs] = '@'
        elif your_board[move] == '*':
            your_board[move] = 'U'

        if self.hidden_moves[me] > len(self.hidden_moves):
            your_board = [x if x != '@' else 'U' for x in your_board]

        self.boards[you] = ''.join(your_board)

        # update real board
        real_board = list(self.boards[2])
        if self.turn == 0 and real_board[move] == '*':
            real_board[move] = self.zero_sym
        elif self.turn == 1 and real_board[move] == '*':
            real_board[move] = self.one_sym

        self.boards[2] = ''.join(real_board)

        # if cpu's turn, print decision
        if self.turn == 1:
            if move in self.hidden_spaces:
                print('computer moves to hidden spot'.format(move))
            else:
                print('computer moves to spot {}'.format(move))

        # determine if the game is over
        self.end = self.is_end()

        # determine whose turn is next
        self.turn = (self.turn + 1) % 2

    ## get_my_board
    # returns the board from the point of view of the player
    #
    # @param player - (int) a 0 or 1 representing which player is
    #                 requesting the board: 0 for user, 1 for cpu
    # @param [out] b - (str) a string representation of the board
    #                  using the encoding in create_matrices
    #
    def get_my_board(self, player):
        me = player
        you = (player + 1) % 2
        b = ''.join([str(move) for move in self.moves[me]])
        b += '_'
        for move in self.moves[you]:
            if move in self.hidden_spaces:
                b += '?'
            else:
                b += str(move)
        b += '_'
        b += self.boards[me]

        return b

    ## board6
    # returns a string that represents the CTTT board with 6 spaces
    #
    # @param bs - (str) a string of length self.spaces that shows which
    #             player has player where. example: '#X#O*X'
    # @param [out] s - (str) a string that visualizes the board
    #
    def board6(self, bs):
        s = '   | {} | {} |   \n'.format(bs[0], bs[1])
        s += ' {} |       | {} \n'.format(bs[5], bs[2])
        s += '   | {} | {} |   \n'.format(bs[4], bs[3])
        return s

    ## board5
    # returns a string that represents the CTTT board with 5 spaces
    #
    # @param bs - (str) a string of length self.spaces that shows which
    #             player has player where. example: '#X#O*'
    # @param [out] s - (str) a string that visualizes the board
    #
    def board5(self, bs):
        s = '  {}  \n'.format(bs[0])
        s += '{}   {}\n'.format(bs[4], bs[1])
        s += ' {} {} \n'.format(bs[3], bs[2])
        return s

    ## board8
    # returns a string that represents the CTTT board with 8 spaces
    #
    # @param bs - (str) a string of length self.spaces that shows which
    #             player has player where. example: 'XO#*XO#*'
    # @param [out] s - (str) a string that visualizes the board
    #
    def board8(self, bs):
        s = '   | {} | {} |   \n'.format(bs[0], bs[1])
        s += ' {} |       | {} \n'.format(bs[7], bs[2])
        s += ' {} |       | {} \n'.format(bs[6], bs[3])
        s += '   | {} | {} |   \n'.format(bs[5], bs[4])
        return s
    
    ## board9
    # returns a string that represents the CTTT board with 9 spaces
    #
    # @param bs - (str) a string of length self.spaces that shows which
    #             player has player where. example: 'XO#*XO#**'
    # @param [out] s - (str) a string that visualizes the board
    #
    def board9(self, bs):
        s = '  {} {}  \n'.format(bs[0], bs[1])
        s += '{}     {}\n'.format(bs[8], bs[2])
        s += '{}     {}\n'.format(bs[7], bs[3])
        s += ' {} {} {} \n'.format(bs[6], bs[5], bs[4])
        return s
    
    ## print_me
    # prints out a representation of the board from a neutral perspective
    #
    # @param inst - (bool) when set to True, this will print out a
    #               version of the board that shows the numbering
    #               for each space
    #
    def print_me(self, inst=False):
        l = list(self.boards[2])
        if inst:
            bs = ''.join([str(x) for x in range(self.size)])
        else:
            bs = ''
            for i, s in enumerate(l):
                if i in self.hidden_spaces:
                    bs += '#'
                else:
                    bs += l[i]
        if self.size == 6:
            s = self.board6(bs)
        elif self.size == 5:
            s = self.board5(bs)
        elif self.size == 8:
            s = self.board8(bs)
        elif self.size == 9:
            s = self.board9(bs)
        else:
            s = bs
        print(s)

    ## print_final
    # prints out a representation of the board showing who played where
    # including revealed hidden spaces. this is meant to be shown when the
    # game is over
    def print_final(self):
        bs = self.boards[2]
        if self.size == 6:
            s = self.board6(bs)
        elif self.size == 5:
            s = self.board5(bs)
        elif self.size == 8:
            s = self.board8(bs)
        elif self.size == 9:
            s = self.board9(bs)
        else:
            s = bs
        print(s)

    ## is_end
    # determine whether the game is over
    #
    # @param [out] end - (bool) True if the game is over and False otherwise
    #
    # Note that this also sets the self.res property to reflect if someone
    # won (1) or if the game ended in a tie (0)
    #
    def is_end(self):
        board = self.boards[2]

        # tie game
        if '*' not in board:
            self.res = 0
            end = True
        else:
            end = False

        for i in range(self.size):
            sm = ''
            for j in range(self.in_a_row):
                sm += board[(i + j) % self.size]

            # the current player wins/loses
            if sm in self.end_strs:
                self.res = 1
                end = True
                break

        return end
