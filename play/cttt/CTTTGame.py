# a circular tic-tac-toe game
from cttt.CTTTGameBoard import CTTTGameBoard
from cttt.CTTTUserPlayer import CTTTUserPlayer
from cttt.CTTTCpuPlayer import CTTTCpuPlayer
from TTTGame import TTTGame


class CTTTGame(TTTGame):

    def __init__(self, size, hidden=[], in_a_row=3, wins=True, display_strategy=False):
        self.board = CTTTGameBoard(size, hidden, in_a_row, wins)
        self.user_player = CTTTUserPlayer()
        w_or_l = 'w'
        if not wins:
            w_or_l = 'l'
        self.cpu_player = CTTTCpuPlayer('cttt/cttt{}h{}{}{}_policy.json'
                                        .format(size, ''.join([str(x) for x in hidden]), w_or_l, in_a_row), \
                                        size, display_strategy)
        self.size = size
        self.in_a_row = in_a_row
        self.wins = wins

    def print_rules(self):
        if self.wins:
            res_str = 'wins'
        else:
            res_str = 'loses'

        s = 'Welcome to playing circular Tic-Tac-Toe.\n'
        s += 'The game board is a circle with {} spaces.\n'.format(self.size)
        s += 'The player with {} in a row '.format(self.in_a_row)
        s += 'on the board {},\n'.format(res_str)
        s += 'and the game ends in a tie if the current player '
        s += 'cannot play anywhere.\n'
        s += 'On each turn you will enter the space you would like to play as '
        s += 'a number between 0 and {}:'.format(self.size - 1)
        print(s)
        self.board.print_me(inst=True)
