# a circle tic-tac-toe user player
from TTTPlayer import TTTPlayer


class CTTTUserPlayer(TTTPlayer):

    def get_play(self, board):
        board.print_me()
        inpt = input('where would you like to play? ')
        return int(inpt)
