# a circular tic-tac-toe CPU player
from TTTPlayer import TTTPlayer
import json
import random
import numpy as np
import os.path
import itertools as it

class CTTTCpuPlayer(TTTPlayer):

    ## __init__
    # intializes the class
    #
    # @param fs - (str) path to a JSON file with a policy
    # @param sz - (int) the size of the game board (so far only used for the
    #             random player)
    # @param display_strategy - (bool) if True, during play, the policy of this player will
    #                           print out to the screen
    #
    # this function also reads in the JSON policy file and sets self.policy to be
    # read during game play
    #
    def __init__(self, fs, sz, display_strategy):
        self.sz = sz
        if os.path.isfile(fs):
            f = open(fs, 'r')
            self.policy = json.load(f)
            f.close()
        else:
            print('warning: no policy found - using random CPU player with {} spaces'.format(sz))
            self.policy = None
        self.display_strategy = display_strategy

    def get_play(self, board):
        # if there is no policy, we can just play randomly
        if self.policy is None:
            move = random.choice(range(self.sz))
            while not np.isnan(board.board[move]):
                move = random.choice(range(self.sz))
        
        # otherwise play according to the policy
        else:
            m = self.policy[board.get_my_board(1)]
            l = list(m.items())
            if self.display_strategy:
                print('cpu strategy: {}'.format(m))
            moves = [int(x[0]) for x in l]
            pcts = [x[1] for x in l]
            rnd = random.random()
            i = 0
            acc = 0
            for pct in pcts:
                acc += pct
                if rnd < acc:
                    break
                else:
                    i += 1
            move = moves[i]

        return move
    
    
