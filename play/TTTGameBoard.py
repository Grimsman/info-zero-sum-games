# a generic game board for tic-tac-toe


class TTTGameBoard:

    def __init__(self):
        self.board = None
        self.end = False
        self.turn = 0
        self.res = 0
        self.zero_sym = 'X'
        self.one_sym = 'O'

    def update(self, play):
        pass

    def print_me(self):
        pass
    
    def print_final(self):
        pass

    def is_end(self):
        pass
