# the game board for standard tic-tac-toe
from TTTGameBoard import TTTGameBoard
import numpy as np


class STTTGameBoard(TTTGameBoard):

    def __init__(self):
        TTTGameBoard.__init__(self)
        self.board = np.empty(9)
        self.board[:] = np.nan

    def update(self, play):
        if np.isnan(self.board[play]):
            self.board[play] = self.turn
        else:
            print('error: move not valid')

        self.end = self.is_end()

        if self.turn == 0:
            self.turn = 1
        else:
            self.turn = 0

    def get_board_str(self):
        s = ''
        for i in self.board:
            if np.isnan(i):
                s+=' '
            elif i == 1:
                s+= self.one_sym
            elif i == 0:
                s+= self.zero_sym
        return s

    def print_me(self):
        bs = self.get_board_str()
        s = ' {} | {} | {} \n'.format(bs[0], bs[1], bs[2])
        s+= '---+---+---\n'
        s+= ' {} | {} | {} \n'.format(bs[3], bs[4], bs[5])
        s+= '---+---+---\n'
        s+= ' {} | {} | {} \n'.format(bs[6], bs[7], bs[8])
        print(s)
    
    def print_final(self):
        self.print_me()

    def is_end(self):

        #tie game
        if not np.isnan(self.board).any():
            self.res = 0
            return True

        b = np.copy(self.board).reshape((3, 3))
        res = False
        diag1_sum = b[0, 0] + b[1, 1] + b[2, 2]
        diag2_sum = b[0, 2] + b[1, 1] + b[2, 0]

        # player 0 wins
        if diag1_sum == 0 or diag2_sum == 0 or \
                True in (b.sum(axis=0) == 0) \
                or True in (b.sum(axis=1) == 0):
            self.res = -1
            return True

        # player 1 wins
        if diag1_sum == 3 or diag2_sum == 3 or \
                True in (b.sum(axis=0) == 3) \
                or True in (b.sum(axis=1) == 3):
            self.res = 1
            return True

        return False
