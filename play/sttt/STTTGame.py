# the standard tic-tac-toe game
from TTTGame import TTTGame
from sttt.STTTGameBoard import STTTGameBoard
from sttt.STTTUserPlayer import STTTUserPlayer
from sttt.STTTCpuPlayer import STTTCpuPlayer


class STTTGame(TTTGame):

    def __init__(self):
        self.board = STTTGameBoard()
        self.user_player = STTTUserPlayer()
        self.cpu_player = STTTCpuPlayer('sttt/sttt_policy.json')
