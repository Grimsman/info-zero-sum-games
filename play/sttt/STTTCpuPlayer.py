# a standard tic-tac-toe CPU player
from TTTPlayer import TTTPlayer
import json
import numpy as np
import random


class STTTCpuPlayer(TTTPlayer):

    def __init__(self, fs):
        f = open(fs, 'r')
        self.policy = json.load(f)
        f.close()

    def board_str(self, board):
        v = board.board.reshape(9)
        s = ''
        for i in v:
            if np.isnan(i):
                s += 'C'
            elif i == 0:
                s += 'A'
            elif i == 1:
                s += 'B'
        return s

    def get_play(self, board):
        bs = self.board_str(board)
        best_moves = self.policy[bs]['best_moves']
        move_coord = random.choice(best_moves)
        move = move_coord[0] * 3 + move_coord[1]
        print('computer moves to spot {}'.format(move))
        return move
