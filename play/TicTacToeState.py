# this class captures the state of the board for a tic tac toe game
import numpy as np


class TicTacToeState:

    def __init__(self, mat, turn, val=[-np.inf, -np.inf]):
        self.mat = mat
        self.turn = turn
        self.val = val
        self.places_to_move = np.argwhere(np.isnan(mat))
        self.name = self.create_name()
        self.best_moves = []

    def get_move_to(self, other):
        my_open_spaces = np.argwhere(np.isnan(self.mat))
        other_open_spaces = np.argwhere(np.isnan(other.mat))
        for i, my_open_space in enumerate(my_open_spaces):
            if len(other_open_spaces) <= i:
                return my_open_space
            other_open_space = other_open_spaces[i]
            if (my_open_space != other_open_space).any():
                return my_open_space

    def create_name(self):
        name = ''
        for i in range(3):
            for j in range(3):
                val = self.mat[i, j]
                if val == 0:
                    name += 'A'
                elif val == 1:
                    name += 'B'
                else:
                    name += 'C'
        return name

    def __eq__(self, other):
        if self.mat == other.mat and self.turn == other.turn:
            return True
        return False

    # iterates through the next possible states
    def next_state(self):
        if np.size(self.places_to_move) == 0:
            return None
        move = self.places_to_move[-1]
        new_mat = self.mat.copy()
        new_mat[move[0], move[1]] = self.turn
        if self.turn == 1:
            new_turn = 0
        else:
            new_turn = 1
        self.places_to_move = self.places_to_move[:-1]
        return TicTacToeState(new_mat, new_turn)

    # determines whether this is an end state or not
    def is_end(self):
        res = False
        diag1_sum = self.mat[0, 0] + self.mat[1, 1] + self.mat[2, 2]
        diag2_sum = self.mat[0, 2] + self.mat[1, 1] + self.mat[2, 0]

        # player 0 wins
        if diag1_sum == 0 or diag2_sum == 0 or \
                True in (self.mat.sum(axis=0) == 0) \
                or True in (self.mat.sum(axis=1) == 0):
            self.val = [1, -1]
            res = True

        # player 1 wins
        elif diag1_sum == 3 or diag2_sum == 3 or \
                True in (self.mat.sum(axis=0) == 3) \
                or True in (self.mat.sum(axis=1) == 3):
            self.val = [-1, 1]
            res = True

        # tie game
        elif not np.isnan(self.mat.sum()):
            self.val = [0, 0]
            res = True

        return res

    def __str__(self):
        s = 'Tic-Tac-Toe State ' + self.name
        s += '\nmatrix: \n{}'.format(self.mat)
        s += '\nturn: {}'.format(self.turn)
        s += '\nvalue: {}'.format(self.val)
        return s

    def __repr__(self):
        return self.__str__()
