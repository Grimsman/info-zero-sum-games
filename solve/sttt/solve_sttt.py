# this is a script that will solve the tic tac toe game, so that a player can
# know what to do for any given time step
from TicTacToeState import TicTacToeState
import numpy as np
import json

# initial state (player 0 always gets to go first)
mat = np.empty((3, 3))
mat[:] = np.nan
turn = 0
init_state = TicTacToeState(mat, turn)
states_to_process = [init_state]
states = {}
for_tree = {}
back_tree = {}
end_states = []

print('beginning process...')
count = 0
while len(states_to_process) > 0:
    state = states_to_process[0]
    states_to_process = states_to_process[1:]
    # print('states to process{}'.format(states_to_process))
    if state.name in for_tree:
        continue
    states[state.name] = state
    if state.is_end():
        end_states.append(state.name)
        continue
    for_tree[state.name] = []
    nx = state.next_state()
    while nx:
        for_tree[state.name].append(nx.name)
        states_to_process.append(nx)
        if nx.name not in back_tree:
            back_tree[nx.name] = []
        back_tree[nx.name].append(state.name)
        nx = state.next_state()

    if count % 1000 == 0:
        print('done with {} nodes'.format(count))
    count += 1

print('done')


# fill in values for all states
def find_val(name):
    state = states[name]
    if state.val != [-np.inf, -np.inf]:
        return state.val
    max_val = [-np.inf, -np.inf]
    max_moves = []
    for nx_name in for_tree[name]:
        val = find_val(nx_name)
        if val[state.turn] > max_val[state.turn]:
            max_val = val
            max_moves = [state.get_move_to(states[nx_name])]
        elif val[state.turn] == max_val[state.turn]:
            max_moves.append(state.get_move_to(states[nx_name]))
    states[name].val = max_val
    states[name].best_moves = max_moves
    return max_val

print('filling in values for states...')
find_val('CCCCCCCCC')
print('done')

print(states['CCCCCCCCC'])

#f = open('ttt_moves.csv', 'w')
#f.write('state\tbest_moves\tis_end')
d = {}
for name, state in states.items():
    best_moves = [x.tolist() for x in state.best_moves]
    d[name] = {'best_moves':best_moves, 'is_end':state.is_end(),
               'val':state.val}
    #f.write('\n{}\t{}\t{}'.format(name, state.best_moves, state.is_end()))
#f.close()
with open('sttt_policy.json', 'w') as f:
    json.dump(d, f)
