% make sure that there is exactly one -1 in each row except the top
s = sum(F == -1, 2);
assert(max(s) == 1)
assert(min(s) == 0)
assert(s(1) == 0)
assert(sum(s - 1) == -1)

s = sum(E == -1, 2);
assert(max(s) == 1)
assert(min(s) == 0)
assert(s(1) == 0)
assert(sum(s - 1) == -1)

% make sure that there is at least one 1 in each row
s = sum(F, 2);
assert(min(s) >= 0)

s = sum(E, 2);
assert(min(s) >= 0)

% make sure that F could stand alone
[n, m] = size(F);
f = sparse(m, 1);
beq = sparse(n, 1);
beq(1) = 1;
linprog(f, [], [], F, beq);

% make sure that E could stand alone
[n, m] = size(E);
f = sparse(m, 1);
beq = sparse(n, 1);
beq(1) = 1;
linprog(f, [], [], E, beq);

disp('all tests have passed')

