clear
load('cttt.mat')

% solve the primal for player 2's strategy
[nE, ~] = size(E);
[~, mA] = size(A);
f = sparse(nE + mA, 1);
f(1) = 1;

Z1 = sparse(mA, nE);
I = speye(mA);
Aub = [-E' A; Z1, -I];
[nAub, ~] = size(Aub);
b = zeros(nAub, 1);

[nF, mF] = size(F);
Z2 = sparse(nF, nE);
Aeq = [Z2 F];
beq = sparse(nF, 1);
beq(1) = 1;

%options = optimoptions(@linprog, 'Algorithm', 'interior-point');
options = optimoptions(@linprog, 'Algorithm', 'dual-simplex', ...
    'Display', 'iter');%, 'MaxIter', 1e7);
tic
uy = linprog(f, Aub, b, Aeq, beq, [], [], [], options);
toc
u = uy(1:nE);
y = uy(nE + 1: nE + mA);

%solve the dual for player 1's strategy
[nF, ~] = size(F);
[nE, mE] = size(E);
f = sparse(nF + mE, 1);
f(1) = 1;

Z1 = sparse(mE, nF);
I = speye(mE);
Aub = [F' -A'; Z1 -I];
b = sparse(mF + mE, 1);

Z2 = sparse(nE, nF);
Aeq = [Z2 E];
beq = sparse(nE, 1);
beq(1) = 1;

tic
vx = linprog(-f, Aub, b, Aeq, beq, [], [], [], options);
toc
v = vx(1:nF);
x = vx(nF + 1:mE + nF);

disp(strcat({'value of game for player 1: '}, num2str(u(1))))
disp(strcat({'value of game for player 2: '}, num2str(v(1))))

save('policy.mat', 'x', 'y') 

