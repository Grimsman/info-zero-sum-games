import matlab.engine
import copy
from scipy.sparse import lil_matrix
from scipy.io import savemat, loadmat
import json
import os
import random

# game design
NUM_SPACES = 3
IN_A_ROW = 2
WINS = True
HIDDEN_SPACES = []
EPSILON = 0
INIT_SZ_R = 2000000
INIT_SZ_C = 2000000
REDUCE_GAME = False
REDUCE_TO = 0.25
CREATE_NETWORK_FILES = True

w_str = 'loses'
if WINS:
    w_str = 'wins'
print('running CTTT with {} spaces, where {} in a row {}'
      .format(NUM_SPACES, IN_A_ROW, w_str))
print('hidden spaces: {}'.format(HIDDEN_SPACES))
if REDUCE_GAME:
    print('reducing game by {}%'.format(REDUCE_TO*100))
if CREATE_NETWORK_FILES:
    print('creating network files as we go')


class infoset:

    ## __init__
    #
    # initializes the class: parameters as defined above
    #
    def __init__(self, player, strat, strat_u, count, label, prev_num):
        self.player = player
        self.strat = strat
        self.strat_u = strat_u
        self.count = count
        self.label = label
        self.prev_num = prev_num

    ## flip_rotate
    # return a flipped and rotated version of the current information set. NOTE: this
    # function is not needed unless we are implementing some kind of symmetry detection.
    #
    # @param flip - (bool) represents whether to flip the current information set
    # @param rotate - (int between 0 and NUM_SPACES - 1) how many spaces to rotate the current
    #                 information set
    # @param[out] new_info - an infoset that is the flipped and rotated version of
    #                        the current information set
    #
    def flip_rotate(self, flip, rotate):
        new_strat = copy.copy(self.strat)
        new_strat_u = copy.copy(self.strat_u)
        new_label = self.label
        if flip:
            new_strat = [NUM_SPACES - x for x in new_strat]
            new_strat_u = [NUM_SPACES - x if isinstance(x, int) else x
                           for x in new_strat_u]
            new_label = new_label[::-1]
        new_strat = [(x - rotate) % NUM_SPACES for x in new_strat]
        new_strat_u = [(x - rotate) % NUM_SPACES if isinstance(x, int) else x
                       for x in new_strat_u]
        new_label = new_label[rotate:] + new_label[:rotate]
        return infoset(self.player, new_strat, new_strat_u, self.count,
                       new_label, self.prev_num)

    ## __str__
    #
    # print an instance of the class
    #
    def __str__(self):
        s = str(self.player)
        s += '_'
        s += self.strat_str()
        s += '_'
        s += ''.join([str(x) for x in self.strat_u])
        s += '_'
        s += self.label
        return s

    ## strat_str
    #
    # print the player's strategy prior to arriving at the information set
    #
    def strat_str(self):
        return ''.join([str(x) for x in self.strat])


class state:

    ## __init__
    #
    # initializes the class: parameters as shown above
    #
    def __init__(self, player_i, player_u, info_i, info_u, sym,
                 real_state, prev_state):
        self.player_i = player_i
        self.player_u = player_u
        self.info_i = info_i
        self.info_u = info_u
        self.sym = sym
        self.real_state = real_state
        self.prev_state = prev_state

    ## __str__
    #
    # represents the state as a str
    #
    def __str__(self):
        strat_i = self.info_i.strat
        strat_u = self.info_u.strat
        if player_i == 1:
            s = ''.join([str(x) for x in strat_i]) + '_'\
                + ''.join([str(x) for x in strat_u])
        else:
            s = ''.join([str(x) for x in strat_u]) + '_'\
                + ''.join([str(x) for x in strat_i])
        return s

    ## get_score
    #
    # retrieves the score of the game, returning one of the following values:
    #  1   (player 1 wins)
    # -1   (player 2 wins)
    #  0   (tie)
    # None (game not over)
    #
    def get_score(self):
        res = None
        if self.sym == 'X':
            s = 'O'
        else:
            s = 'X'
        end_str = ''.join([s for x in range(IN_A_ROW)])
        if '*' not in self.real_state:
            res = 0
        for i in range(NUM_SPACES):
            s = ''
            for j in range(IN_A_ROW):
                s += self.real_state[(i + j) % NUM_SPACES]
            if s == end_str:
                num_moves = len(self.info_i.strat) + len(self.info_u.strat)
                if self.player_u == 1:
                    res = 1 - num_moves * EPSILON
                else:
                    res = -1 + num_moves * EPSILON
                break
        if res is not None and not WINS:
            res *= -1
        return res

    ## apply_move_to_me
    #
    # applies the move to the infoset, assuming that the move is being made by the player who owns the infoset
    #
    # move - (int) a number 0:N-1, indicating where the player would like to play
    # info - (infoset) the information set of the player who is making the move
    # num - (int) the number associated with the current infoset
    #
    # returns the new infoset
    #
    def apply_move_to_me(self, move, info, num):
        i = list(info.label)
        if info.label[move] == '*':
            i[move] = 'I'
        elif info.label[move] == '@':
            i[move] = '$'
        else:
            i = ['']
        si = list(info.strat)
        si.append(move)
        return infoset(info.player, si, copy.copy(info.strat_u), info.count, ''.join(i), num)

    ## apply_move_to_you
    #
    # applies the move to the infoset, assuming that the move is being made by
    # the opponent of the player who owns the infoset
    #
    # move - (int) a number 0:N-1, indicating where the player would like to play
    # info - (infoset) the information set of the opponent of the player who is making the move
    #
    # returns the new infoset
    #
    def apply_move_to_you(self, move, info):
        i = list(info.label)
        strat_u = copy.copy(info.strat_u)
        c = info.count
        if move in HIDDEN_SPACES:
            if c == 0:
                for hs in HIDDEN_SPACES:
                    if i[hs] != 'I':
                        i[hs] = '@'
            c += 1
            strat_u.append('?')
        elif i[move] == '*':
            i[move] = 'U'
            strat_u.append(move)
        else:
            i = ['']

        if c == len(HIDDEN_SPACES):
            i = [x if x != '@' else 'U' for x in i]

        return infoset(info.player, copy.copy(info.strat), strat_u, c, ''.join(i), info.prev_num)

    ## apply_move_to_real
    #
    # apply the move to the real game board
    #
    # move - (int) a number 0:N-1, indicating where player_i would like to play
    #
    def apply_move_to_real(self, move):
        nx_real = list(self.real_state)
        if nx_real[move] == '*':
            nx_real[move] = self.sym
        return ''.join(nx_real)

    ## get_next_states
    #
    # get a list of all possible states that could follow the current state. if the current state is an end state
    # this will be an empty list
    #
    # info_num - (int) the number associated with info_i
    #
    def get_next_states(self, info_num):
        moves_left = [i for i in range(NUM_SPACES) if self.info_i.label[i] in ['@', '*']]
        nx_states = []
        if self.sym == 'X':
            nx_sym = 'O'
        else:
            nx_sym = 'X'
        for move in moves_left:
            ii = self.apply_move_to_me(move, self.info_i, info_num)
            iu = self.apply_move_to_you(move, self.info_u)
            nx_real = self.apply_move_to_real(move)
            st = state(self.player_u, self.player_i, iu, ii, nx_sym,
                       nx_real, str(self))
            if iu.label != '' and ii.label != '':
                nx_states.append(st)
        return nx_states


# update E or F matrix
def update_tree_matrix(M, row, col, par_row):
    if par_row != -1:
        M[par_row, col] = 1
    if row:
        M[row, col] = -1


# store data in dicts
def store(data, data_cnt, player, val, nums):
    if val in data[player]:
        num = data[player][val]
    else:
        data[player][val] = data_cnt[player]
        num = data_cnt[player]
        nums[player][num] = val
        data_cnt[player] += 1
    return num

# create matrices for linear program
# need to find matrices E, F, and A
# each will be a sparse scipy matrix

# create list of states while adding to E F and A
E = lil_matrix((INIT_SZ_R, INIT_SZ_C))
F = lil_matrix((INIT_SZ_R, INIT_SZ_C))
A = lil_matrix((INIT_SZ_C, INIT_SZ_C))

# put initial state on stack
lbl = ''.join(['*' for x in range(NUM_SPACES)])
info1 = infoset(1, [], [], 0, lbl, 0)
info2 = infoset(2, [], [], 0, lbl, 0)
st = state(1, 2, info1, info2, 'X', lbl, '')
states_to_process = [st]

# initialize dicts
infos = {1: {}, 2: {}}
info_nums = {1: {0: ''}, 2: {0: ''}}
info_cnt = {1: 1, 2: 1}
strats = {1: {}, 2: {}}
strat_nums = {1: {0: ''}, 2: {0: ''}}
strat_cnt = {1: 0, 2: 0}

# initialiex other parameters
print('creating E, F, and A matrices')
tot_moves = NUM_SPACES + len(HIDDEN_SPACES) - 1
if len(HIDDEN_SPACES) == 0:
    tot_moves += 1
pct = REDUCE_TO**(1 / float(tot_moves))
if CREATE_NETWORK_FILES:
    f_edge = open('cttt_strategic_form_network_edges.csv', 'w')
    f_edge.write('Source,Target,Label,InfoSet')
    f_node = open('cttt_strategic_form_network_nodes.csv', 'w')
    f_node.write('Id,Label,Player')

# while there are states to process
while states_to_process:

    # pick the state off the top of the stack
    st = states_to_process.pop()
    if REDUCE_GAME and random.random() > pct:
        # get how far into the tree it is
        # l = split(str(st.info_i), '_')
        # num_moves = len(l[1]) + len(l[2])
        # if random.random() < REDUCE_TO**()
        # select True or False probabilistically
        continue
    player_i = st.player_i
    player_u = st.player_u
    s = ''
    if st.info_u.strat:
        s = str(st.info_u.strat[-1])
    strat = info_nums[player_i][st.info_i.prev_num] + '_' + s
    info = str(st.info_i)
    if CREATE_NETWORK_FILES:
        f_edge.write('\n{},{},{},{}'.format(st.prev_state, str(st), s, info))
        f_node.write('\n{},{},{}'.format(str(st), str(st), player_i))
    s = ''
    if st.info_u.strat:
        s = str(st.info_u.strat[-1])
    strat_u = info_nums[player_u][st.info_u.prev_num] + '_' + s

    # find if the information set corresponds to a stratgey that has already been stored
    strat_num = store(strats, strat_cnt, player_i, strat, strat_nums)

    # if it's a terminal state, update A, otherwise, add the information set to the list and retrieve future states
    score = st.get_score()
    if score is not None:
        store(strats, strat_cnt, player_u, strat_u, strat_nums)
        if player_i == 1:
            row = strats[1][strat]
            col = strats[2][strat_u]
        else:
            row = strats[1][strat_u]
            col = strats[2][strat]
        A[row, col] = score
        info_num = None

        # update the tree matrix for other player
        par_row = st.info_u.prev_num
        col = strats[player_u][strat_u]
        if player_u == 1:
            update_tree_matrix(E, None, col, par_row)
        else:
            update_tree_matrix(F, None, col, par_row)
    else:
        info_num = store(infos, info_cnt, player_i, info, info_nums)
        for nxt_state in st.get_next_states(info_num):
            states_to_process.append(nxt_state)

    # update E or F, depending on whose turn it is
    par_row = st.info_i.prev_num
    row = info_num
    col = strat_num
    if player_i == 1:
        update_tree_matrix(E, row, col, par_row)
    else:
        update_tree_matrix(F, row, col, par_row)

if CREATE_NETWORK_FILES:
    f_node.close()
    f_edge.close()

# trim the E,F,A matrices

E = E[:info_cnt[1], :strat_cnt[1]]
F = F[:info_cnt[2], :strat_cnt[2]]
A = A[:strat_cnt[1], :strat_cnt[2]]

print('size of E: {}x{}'.format(E.shape[0], E.shape[1]))
print('size of F: {}x{}'.format(F.shape[0], F.shape[1]))
print('size of A: {}x{}'.format(A.shape[0], A.shape[1]))

# reduce the matrices if necessary
#def red_EF(red_pct, player):
#    num_sel_strats = round(strat_cnt[player]*red_pct)
#    sel_strat_nums = sorted(random.sample(strat_nums[player].keys(),
#                                          int(num_sel_strats)))
#    if player == 1:
#        mat = E
#    else:
#        mat = F
#    return mat[:, sel_strat_nums]


#def red_A(red_pct1, red_pct2):
#    A_new = copy.copy(A)
#    num_sel_strats = round(strat_cnt[1]*red_pct1)
#    sel_strat_nums = sorted(random.sample(strat_nums[1].keys(),
#                                          int(num_sel_strats)))
#    A_new = A_new[sel_strat_nums, :]
#    num_sel_strats = round(strat_cnt[2]*red_pct2)
#    sel_strat_nums = sorted(random.sample(strat_nums[2].keys(),
#                                          int(num_sel_strats)))
#    A_new = A_new[:, sel_strat_nums]
#    return A_new


#if REDUCE_GAME:
#    print('reducing matrices')
#    E1 = red_EF(REDUCE_TO[1]['me'], 1)
#    F1 = red_EF(REDUCE_TO[1]['you'], 2)
#    A1 = red_A(REDUCE_TO[1]['me'], REDUCE_TO[1]['you'])
#    E2 = red_EF(REDUCE_TO[2]['you'], 1)
#    F2 = red_EF(REDUCE_TO[2]['me'], 2)
#    A2 = red_A(REDUCE_TO[2]['you'], REDUCE_TO[2]['me'])

# save matrices to MATLAB format
print('saving matrices to file')
#if REDUCE_GAME:
#    savemat('MATLAB/cttt_red.mat', {'E1': E1, 'E2': E2, 'F1': F1, 'F2': F2,
#                                    'A1': A1, 'A2': A2})
#else:
savemat('MATLAB/cttt.mat', {'E': E, 'F': F, 'A': A})

# run matlab script to solve linprog
print('running MATLAB script')
os.chdir('MATLAB')
eng = matlab.engine.start_matlab()
# if REDUCE_GAME:
#    eng.solve_cttt_red(nargout=0)
#else:
eng.solve_cttt(nargout=0)
os.chdir('..')

# import policy from MATLAB and save as JSON
if REDUCE_GAME:
    quit()
print('creating policy')
d = loadmat('MATLAB/policy.mat')
policy = {}
for player in range(1, 3):
    if player == 1:
        M = E
        sym = 'x'
    else:
        M = F
        sym = 'y'
    for info, info_num in infos[player].items():
        info = info[2:]
        if info in policy:
            continue
        policy[info] = {}
        strat_inds = M[info_num, :].nonzero()[1][1:]
        tot = 0
        for strat_ind in strat_inds:
            tot += d[sym][strat_ind][0]
        for strat_ind in strat_inds:
            strat = strat_nums[player][strat_ind]
            move = int(strat[-1])
            if tot != 0:
                policy[info][move] = float(d[sym][strat_ind][0]) / tot
            else:
                policy[info][move] = float(d[sym][strat_ind][0])
w_or_l = 'w'
if not WINS:
    w_or_l = 'l'
with open('../../play/cttt/cttt{}h{}{}{}_policy.json'
          .format(NUM_SPACES, ''.join(str(x) for x in HIDDEN_SPACES), w_or_l,
                  IN_A_ROW), 'w') as f:
    json.dump(policy, f)
